const {app, BrowserWindow, protocol} = require('electron');
const path = require('path');
const url = require('url');
const twig = require('twig');
twig.cache(false);
let win;
var os = require('os');
var fs = require('fs');
var mime = require('mime');
var os_vars = {
	hostname: os.hostname(),
	username: os.userInfo()['username']
};
function createWindow () {
	protocol.interceptBufferProtocol('file', function (request, callback) {
		var reqPath = path.normalize(request.url.substr(7));
		if(reqPath.startsWith("\\") && os.type() == "Windows_NT") {
			reqPath = reqPath.substring(1);
		}
		if (path.extname(reqPath) === '.twig') {
			twig.renderFile(reqPath, {
				os: os_vars
			}, function(err, html) {
				if(err) {
					console.log(err);
				}
				callback({ mimeType: 'text/html', data: new Buffer(html) });
			});
		} else {
			var file = fs.readFileSync(reqPath);
			callback({ mimeType: mime.lookup(reqPath), data: file })
		}
	}, function (err) {
		if (err) {
			console.error('Failed')
		}
	})

  win = new BrowserWindow({width: 800, height: 600, frame: false, transparent: true});
  win.loadURL(url.format({
    pathname: path.join(__dirname, '/views/index.html.twig'),
    protocol: 'file:',
    slashes: true
  }));

  win.on('closed', () => {
    win = null;
  });

	setTimeout(function () {
		win.maximize();
	}, 1000);
};
app.on('ready', createWindow);
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});
