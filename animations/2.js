function animate2 (self) {
	var animates = [
		"Bonjour, " + os.username,
		"Il semblerait que j'ai réussi à prendre le contrôle de votre ordinateur",
		"Et oui, vous venez d'être infecté par DeathKey",
		"Autrement dit, la clée de la mort.",
		"J'ai accès à tout maintenant.",
		"Je pourrais m'amuser...",
		"Et pourquoi pas aprés tout :)",
		"Maintenant, votre ordinateur m'appartient",
		"Je me demande ce que vous resentez actuellement",
		"Vous vous demandez sûrement comment cela est arrivé?",
		"Soyez plus prudent à l'avenir.",
		"Pourquoi je ne m'amuserais pas un petit peu?"
	];
	function check (animate) {
		var t = animates[animate];
		if(t == "") {
			$("#body").append("<br />");
		} else {
			$("#body").append("<p>" + t + "</p>");
		}

		setTimeout(function () {
			animate++;
			if(animate < animates.length) {
				check(animate);
			} else {
				animate1(animate3);
			}
		}, 2500);
	}

	check(0);
}
