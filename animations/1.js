function animate1 (checkback) {
	$("#body").html("");
	$("#body").css("background-color", "red");
	setTimeout(function () {$("#body").css("background-color", "blue")}, 250);
	setTimeout(function () {$("#body").css("background-color", "green")}, 500);
	setTimeout(function () {$("#body").css("background-color", "yellow")}, 750);
	setTimeout(function () {$("#body").css("background-color", "pink")}, 1000);
	setTimeout(function () {$("#body").css("background-color", "black")}, 1250);
	setTimeout(function () {checkback()}, 1500);
}
