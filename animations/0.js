function animate0 (self) {
	var animates = [
		"",
		"PyVirus v17.18.153",
		"",
		"Checking for file <b>deathkey.py</b>...",
		"File found! Running it...",
		"",
		"LOADING... 0%",
		"LOADING... 11%",
		"LOADING... 27%",
		"LOADING... 65%",
		"LOADING... 98%",
		"Processing virus...",
		"Data transfer done."
	];
	function check (animate) {
		var t = animates[animate];
		if(t == "") {
			$("#body").append("<br />");
		} else {
			$("#body").append("<p>" + t + "</p>");
		}

		setTimeout(function () {
			animate++;
			if(animate < animates.length) {
				check(animate);
			} else {
				animate1(animate2);
			}
		}, 1000);
	}

	check(0);
}
